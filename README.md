# README #

This code is running a simulation of customers going to a coffee bar over a period of five years.

It takes approximately 10 min to run the entire code, the simulation part is taking almost 8 min itself.






Remark: In explanatory.py, to correctly display the amount of food sold by the bar, we have to:

1: Delete this line:
    Coffee = Coffee.fillna('Nothing')
2: Use:
    Coffee = Coffee.dropna()

Most of the print are available in the comments, to visualize any outcome, please refers to the comments below each part of the code.