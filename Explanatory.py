import pandas as pd
import os
from datetime import datetime
import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')

# FIRSTLY WE OPEN THE DATASET WITH AN ABSOLUTE PATH:
outpath = os.path.abspath('./Coffeebar_2013-2017.csv')
Coffee = pd.read_csv(outpath, delimiter=";")

# --------------------------------------------------------------------------------
# Since the data comes from a csv file, they are added as strings
# we need to convert them into datetime to determine later our probabilities
Coffee['TIME'] = pd.to_datetime(Coffee.TIME)
Coffee['quarter'] = Coffee['TIME'].dt.quarter
Coffee['year'] = Coffee['TIME'].dt.year
start = datetime(2013, 1, 1)
end = datetime(2017, 12, 31)
# --------------------------------------------------------------------------------
Coffee = Coffee.fillna('Nothing')
# Coffee = Coffee.drop na() #Use this command to correctly visualize the bar plot for food. and delete the previous one.

#########################################################################################################
# What food and drinks are sold by the coffee bar? How many unique customers did the bar have?
# Make: a bar plot of the total amount of sold foods (plot1) and drinks (plot2) over the five years
#########################################################################################################

# FOR DRINKS:
# Total amount of drinks sold:
total_amount_drinks = Coffee['DRINKS'].count()
print('The total amount of drinks sold is: %s' % total_amount_drinks)

# What drinks are sold by the coffee bar?
bev = Coffee.DRINKS.unique()
print('The coffee bar is selling those kind of beverage: %s' % bev)

# How many drinks were sold by categories
# value_counts() is used to count the frequency of categorical data in a column
number_drinks = Coffee['DRINKS'].value_counts()
print('the bar sold in total: %s' % number_drinks)


# Bar plot of the total amount of sold DRINKS:
viz = Coffee.groupby(['year', 'quarter'])['DRINKS'].count()
viz1 = viz.plot(kind='bar')  # In order to use a bar plot, we have to manipulate numerical values !
# this command creates dates, separated by trimester, we need 20 different dates, because we have 20 obs in viz
dates = pd.date_range(start, end, freq='3BM')
plt.suptitle('Amount of drinks sold by trimester ')
plt.xlabel('Time divided by trimester')
plt.ylabel('Amount of drinks sold (in units)')
plt.ylim(15250, 16000)
plt.show()


# For FOOD:
# Total amount of FOOD sold:
total_amount_food = Coffee['FOOD'].count()
print('The total amount of food sold is: %s' % total_amount_food)

# What types of food are sold by the coffee bar?
dishes = Coffee.FOOD.unique()
print('The coffee bar is selling those kind of dishes: %s' % dishes)

# How many Food were sold by categories:
number_food = Coffee['FOOD'].value_counts()
print('the bar sold in total: %s' % number_food)

# Bar plot of the total amount of sold foods:
viz = Coffee.groupby(['year', 'quarter'])['FOOD'].count()
viz2 = viz.plot(kind='bar')
dates2 = pd.date_range(start, end, freq='3BM')
plt.suptitle('Amount of food sold by trimester ')
plt.xlabel('Time divided by trimester')
plt.ylabel('Amount of food sold (units)')
plt.ylim(8000, 8400)
plt.show()

# --------------------------------------------------------------------------
# How many unique customers does the bar have?
zz = len(set(Coffee.CUSTOMER))
print('The total amount of unique customers is: %s' % zz)
# --------------------------------------------------------------------------

########################################################################################################################
# Determine the average that a customer buys a certain food or drink at any given time
# for example:
# on average, the probability of a customer at 8:00 buying coffee, soda and water is 50%, 30% and 20% and for food ....
########################################################################################################################
# -----------------------------------------------------------------------------------
# 1) we have to count the number of observations per timestamp
# -----------------------------------------------------------------------------------

# Computations for FOOD :
consumption_dish_per_timestamp = Coffee.groupby(Coffee['TIME'].dt.strftime("%H:%M"))['FOOD'].value_counts(sort=False)
amount_food_consumed_per_timestamp = Coffee.groupby(Coffee['TIME'].dt.strftime("%H:%M"))['FOOD'].count()
average_food = consumption_dish_per_timestamp/amount_food_consumed_per_timestamp

# For visualization purpose:
# print (consumption_dish_per_timestamp)
# print (amount_food_consumed_per_timestamp)
# print('the average a customer buy food is: %s' %average_food.tail(10))

# Computations for DRINKS:
consumption_bev_per_timestamp = Coffee.groupby(Coffee['TIME'].dt.strftime("%H:%M"))['DRINKS'].value_counts(sort=False)
amount_drinks_consumed_per_timestamp = Coffee.groupby(Coffee['TIME'].dt.strftime("%H:%M"))['DRINKS'].count()
average_drink = consumption_bev_per_timestamp/amount_drinks_consumed_per_timestamp

#  For visualization purpose:
# print('the consumption of drinks is:%s ' %consumption_bev_per_timestamp.head(12))
# print ('total drinks : %s' %amount_drinks_consumed_per_timestamp.head())
# print('the average a customer buy drinks is: %s' % (average_drink.head(12)))


# -----------------------------------------------------------------------------------
# 2) We create a data frame that will contain all the probabilities by timestamp
# -----------------------------------------------------------------------------------

# THIS WAS A FIRST METHOD, EVEN IF THE CODE IS RIGHT, IT TOOK TOO MUCH TIME TO RUN.

# We create the future row names:
# my_rows_drinks = []
# for i in range(40):
#     my_rows_drinks.append(Coffee['TIME'].dt.strftime("%H:%M")[i])
#
# my_rows_food = []
# for i in range(36,40):
#     my_rows_food.append(Coffee['TIME'].dt.strftime("%H:%M")[i])

#
# #We import our probabilities for drinks into a list:
# list_drinks = []
# for i in range(240):
#      list_drinks.append(average_drink[i])

# #We import our probabilities for Food into a list:
# list_food = []
# for i in range(38,54):
#     list_food.append(average_food[i])
# print(list_food)
# Creation of two separate data frames:
# drink_df = pd.DataFrame(np.array(list_drinks).reshape(40,6),
# columns=('Coffee','Frappucino','Milkshake','Soda','Tea','Water'),index=my_rows_drinks)
# print(drink_df)

# --------------------------------------------------------------------------------
# If we want to plot these data frames and visualize the relative probabilities
# dfp = plt.imshow(drink_df,interpolation = 'hermite')
# dfp.set_xticklabels(['Coffee, ....'])
# plt.colorbar()
# plt.xlabel('Type of drinks')
# plt.ylabel('Probability of consumption')
# plt.title('Probability of consumption by type of drink')
# plt.show()
# --------------------------------------------------------------------------------


# food_df = pd.DataFrame(np.array(list_food).reshape(4,4),columns=('Cookie','Muffin','Pie','Sandwich'),
#  index=my_rows_food)

# print(food_df)

# test = pd.concat([drink_df, food_df],axis=1, join_axes=[drink_df.index])
# test.fillna('Nothing', inplace=True)
# test2= drink_df.append(food_df, ignore_index= True)
# print(test)

# SECOND METHOD:
prob = pd.concat([average_drink, average_food], axis=1)
prob.fillna(0, inplace=True)
prob['PROBABILITY'] = prob['DRINKS'] + prob['FOOD']
del prob['DRINKS']
del prob['FOOD']
print(pd.DataFrame(prob))
