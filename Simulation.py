import numpy as np
import pandas as pd
import random
from Customer import OneTimer, Returner

# Creation of our customers:
customer_type = ['onetimer', 'returner']
customer_prob = [0.8, 0.2]
num_returner = 1
time_list = []
drink_list = []
food_list = []
customers = []
# We create our 1000 returners:
while num_returner <= 1000:
    rand = random.uniform(0, 1)
    if rand <= 0.333:
        custom = Returner('Hipster', 'CID-R' + str(num_returner))
        customers.append(custom)
    else:
        custom = Returner('Regular', 'CID-R' + str(num_returner))
        customers.append(custom)
    num_returner += 1


# We simulate over 5 years
listYear = []
listDay = []
listHour = []
listDrink = []
listFood = []
listBill = []
listId = []
listType = []
listBudget = []
# For each timestamp over the five years:
for year in range(2015, 2020):      # 5 years simulation
    for day in range(365):          # 365 days within a year
        for hour in range(171):     # 171 timestamps within a day
            # Select randomly a customer
            choice = np.random.choice(customer_type, 1, customer_prob)
            if choice == 'onetimer':
                rand = random.uniform(0, 1)
                if rand <= 0.1:
                    custom = OneTimer(100, 'TripAdvisor')
                else:
                    custom = OneTimer(100, 'Regular')
            else:
                rand = random.randint(1, 1000)
                custom = customers[rand-1]

                # Condition: Once a returner doesn't have enough budget, he is replaced by a one-timer
                if custom.budget < 10:
                    rand = random.uniform(0, 1)
                    if rand <= 0.1:
                        custom = OneTimer(100, 'TripAdvisor')
                    else:
                        custom = OneTimer(100, 'Regular')

            # Then we ask the customer to buy something:
            buy = custom.buy(hour)

            # Characteristics of the purchase
            listYear.append(year)
            listDay.append(day+1)
            listHour.append(buy[0])
            listDrink.append(buy[1])
            listFood.append(buy[2])
            listBill.append(buy[3])
            listId.append(buy[4])
            listType.append(custom.typology)
            listBudget.append(buy[5])

# Concatenation of Year, Day and Hour
listTime = []
for i in range(0, listYear.__len__()):
    listTime.append("%s/%s/%s" % (listYear[i], listDay[i], listHour[i]))

# We save the simulation in a Data Frame
DataFrame = pd.DataFrame({'Time': listTime,
                          'Drink': listDrink,
                          'Food': listFood,
                          'Bill': listBill,
                          'Id': listId,
                          'Type': listType,
                          'Budget': listBudget})
# Export the data frame into a csv file
pd.DataFrame.to_csv(DataFrame, 'Simulation.csv')

print('end of the program')
