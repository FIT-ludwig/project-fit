import random
from Explanatory import *
import numpy as np

# To get hours in a H:M format
coffee_time = Coffee["TIME"].dt.strftime("%H:%M")

# We define a vector of prices and products:
food_cat = ['Cookie', 'Muffin', 'Pie', 'Sandwich']
food_vector = [i for i in range(0, 4)]
price_food = [2, 3, 3, 5]

drink_cat = ['Coffee', 'Frappucino', 'Milkshake', 'Soda', 'Tea', 'Water']
drink_vector = [i for i in range(0, 6)]
price_drink = [3, 4, 5, 3, 3, 2]


class IdCreation(object):
    class_counter = 1


class OneTimer(object):
    def __init__(self, budget, typology):
        self.budget = budget
        self.history = [[], [], [], [], [], []]  # datetime, drinks, food, price, Customer-ID
        self.typology = typology
        self.id = 'CID-' + str(IdCreation.class_counter)
        IdCreation.class_counter += 1

    def buy(self, time):
        # probability to buy a certain drink at a given time:
        proba_drink = average_drink[coffee_time[time]]
        # we let the program to choose randomly a drink, given each probability
        drink_position = int(np.random.choice(drink_vector, 1, p=proba_drink))
        # the drink bought is the one with the same position that the index previously picked
        buy_drink = drink_cat[drink_position]
        self.history[1].append(buy_drink)
        self.history[4].append(self.id)
        self.budget -= price_drink[drink_position]
        tip = 0
        if self.typology == "TripAdvisor":
            tip = random.randint(1, 10)

        # We want to be sure that no food are consumed before 11AM
        if time <= 36:
            buy_food = 'Nothing'
            self.budget -= tip
            self.history[2].append(buy_food)
            self.history[3].append(price_drink[drink_position])
        else:
            proba_food = average_food[coffee_time[time]]
            food_position = int(np.random.choice(len(proba_food), 1, p=proba_food))
            buy_food = food_cat[food_position]
            self.history[2].append(buy_food)
            self.budget -= (price_food[food_position] + tip)
            self.history[3].append(price_drink[drink_position] + price_food[food_position])

        self.history[5].append(self.budget)
        self.history[0].append(coffee_time[time])

        last_buy = [self.history[0][-1], self.history[1][-1], self.history[2][-1], self.history[3][-1],
                    self.history[4][-1], self.history[5][-1]]
        return last_buy


class Returner(object):
    def __init__(self, typology, customer_id):
        self.typology = typology
        self.customerID = customer_id
        self.history = [[], [], [], [], [], []]  # datetime, drinks, food, price, budget

        # This code defines the budget of the Returner
        if self.history[5].__len__() > 0:
            self.budget -= str(self.history[5][-1])
        elif self.typology == "Hipster":
            self.budget = 500
        else:
            self.budget = 250

    def buy(self, time):
        # the probabilities to buy drinks at that special datetime
        proba_drink = average_drink[coffee_time[time]]
        # we let the program to choose randomly a drink, given each probability
        drink_position = int(np.random.choice(drink_vector, 1, p=proba_drink))
        # the drink bought is the one with the same position that the index previously picked
        buy_drink = drink_cat[drink_position]

        self.history[1].append(buy_drink)
        self.history[4].append(self.customerID)

        self.budget -= price_drink[drink_position]

        if time <= 36:
            buy_food = 'Nothing'
            self.history[2].append(buy_food)
            self.history[3].append(price_drink[drink_position])
        else:
            proba_food = average_food[coffee_time[time]]
            food_position = int(np.random.choice(len(proba_food), 1, p=proba_food))
            buy_food = food_cat[food_position]
            self.history[2].append(buy_food)
            self.budget -= price_food[food_position]
            self.history[3].append(price_drink[drink_position] + price_food[food_position])

        self.history[5].append(self.budget)
        self.history[0].append(coffee_time[time])
        last_buy = [self.history[0][-1], self.history[1][-1], self.history[2][-1], self.history[3][-1],
                    self.history[4][-1], self.history[5][-1]]
        return last_buy


print('end customer')
